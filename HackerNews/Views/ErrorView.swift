//
//  ErrorView.swift
//  HoneyGoFood
//
//  Created by Piyush Nishant on 18/11/19.
//  Copyright © 2019 Piyush Nishant. All rights reserved.
//

import UIKit
protocol ErrorViewDelegate: class {
    func retryButtonAction()
}
class ErrorView: UIView {
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var retryButton: UIButton!
    weak var delegate: ErrorViewDelegate?
    
    // MARK: - View initilisation methods
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadView()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        loadView()
    }

    private func loadView() {
        Bundle.main.loadNibNamed("ErrorView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        configureView()
    }

    // MARK: - Coinfigure View
    
    /// Configure View with Background color and other details at the initialisation time
    private func configureView() {
        retryButton.backgroundColor = UIColor(hex: "#ED4F28FF")!
        retryButton.setTitleColor(UIColor(hex: "#FFFFFFFF")!, for: .normal)
        retryButton.layer.cornerRadius = 20
    }

    
    /// Update View with Error Message and Retry Button When User Tap on Retry Button we can hit the respective screen API
    /// - Parameters:
    ///   - errorMessage: Message String
    ///   - isButtonShow: Show and hide Retry Button
    func updateView(errorMessage: String, isButtonShow: Bool = false) {
        errorLabel.text = errorMessage
        retryButton.isHidden = !isButtonShow
    }

    @IBAction func retryButtonAction(_ sender: UIButton) {
        delegate?.retryButtonAction()
    }
    

}
