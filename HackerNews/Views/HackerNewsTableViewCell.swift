//
//  HackerNewsTableViewCell.swift
//  HackerNews
//
//  Created by Piyush Nishant on 10/12/19.
//  Copyright © 2019 Piyush Nishant. All rights reserved.
//

import UIKit
import AMShimmer
// MARK: HackerNewsTableViewCellDelegate
protocol HackerNewsTableViewCellDelegate: class {
    func didFinishWithSuccess(topStory: StoryItem, indexPath: IndexPath)
}


class HackerNewsTableViewCell: UITableViewCell {

    @IBOutlet weak var storyTitleLabel: UILabel!
    @IBOutlet weak var storyAUthor: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    
    private var hackerNewsModel: HackerNewsTableViewCellModel?
    
    /// HackerNewsTableViewCellDelegate
    weak var delegate: HackerNewsTableViewCellDelegate?
    private var indexPath: IndexPath?
    
    // MARK: View life cycle method
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureView()
    }
    
    // MARK: - Configure Cell view Appearance
    private func configureView() {
        hackerNewsModel = HackerNewsTableViewCellModel()
        hackerNewsModel?.itemServiceDelegate = self
        accessoryType = UITableViewCell.AccessoryType.disclosureIndicator
    }
    
    // MARK: - Update Cell content data when request is not successfull
    func updateContentView(itemId: Int64, indexPath: IndexPath,isRequestSuccess:Bool = false) {
        /// Reset the cell content so that cached cell data is not showing
       resetCellContent()
       self.indexPath = nil
        guard !isRequestSuccess else { return }
        hackerNewsModel?.getTopStoryItem(itemId: itemId)
        startShimmerEffect()
        self.indexPath = indexPath
    }
    
    // MARK: - Update the cell content when Request is success for particular cell
    func updateView(topStory: StoryItem?, indexPath: IndexPath) {
        /// Reset the cell content so that cached cell data is not showing
        resetCellContent()
        self.indexPath = indexPath
        if let storyItem = topStory  {
            storyTitleLabel.text = hackerNewsModel?.getTitle(title: storyItem.title)
            storyAUthor.text = hackerNewsModel?.getAuthorName(authorName: storyItem.author)
            commentLabel.text = hackerNewsModel?.getComment(comments: storyItem.comments)
        } else {
            resetCellContent()
        }
    }
    
    // MARK: Reset Cell content
    private func resetCellContent() {
        storyTitleLabel.text = ""
        storyAUthor.text = ""
        commentLabel.text = ""
    }
    
    // MARK: Start the shimmer effect
    private func startShimmerEffect() {
        AMShimmer.start(for: storyTitleLabel)
        AMShimmer.start(for: storyAUthor)
        AMShimmer.start(for: commentLabel)
    }
    
    // MARK: Stop the shimmer effect
    private func stopShimmerEffect() {
        AMShimmer.stop(for: storyTitleLabel)
        AMShimmer.stop(for: storyAUthor)
        AMShimmer.stop(for: commentLabel)
    }
}

// MARK: - Extension for Service delegate
extension HackerNewsTableViewCell: ModelServiceDelegate {
    
    // MARK: Gateway error
    func gateWayError() {
        resetCellContent()
        stopShimmerEffect()
    }
    
    // MARK: Request finish witgh error message
    func didFinishWithError(errorMessage: String) {
        resetCellContent()
        stopShimmerEffect()
    }
    
    // MARK: Request finished with success
    func didFinishWithSuccess(object: CodableInit?) {
        guard let storyItem = object as? StoryItem, let indexPath = self.indexPath else {
            resetCellContent()
            return
        }
        stopShimmerEffect()
        /// Notify parent class 
        delegate?.didFinishWithSuccess(topStory: storyItem, indexPath: indexPath)
    }
}
