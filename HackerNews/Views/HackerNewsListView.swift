//
//  HackerNewsListView.swift
//  HackerNews
//
//  Created by Piyush Nishant on 10/12/19.
//  Copyright © 2019 Piyush Nishant. All rights reserved.
//

import UIKit

// MARK: - Protocol for News List View which notify the Viewccontroller for API Response
protocol HackerNewsListViewDelegate: class {
    func success(errorMessgae: String?)
    func gateWayError()
    func didSelectStory(storyItem: StoryItem)
}

class HackerNewsListView: UIView, InstantiateFromXIBRepresentable {
    @IBOutlet weak var newsListTableView: UITableView!
    @IBOutlet var contentView: UIView!
    private let newsListViewModel = NewsListViewModel()
    weak var delegate: HackerNewsListViewDelegate?
    /// Store Top stories Id to hit the Story details
    private var topStoryIds = [Int64]()
    /// Check if a request is success or not at run time
    private var cellDataRequestStatus = [Int: Bool]()
    /// Store the All response for Story Details
    private var topStories = [Int:StoryItem]()
    /// Used for Filter story items with title
    private var currentTopStories = [StoryItem]()
    /// Check User enter the Text for filter the story
    private var isFilter = false

    /// Pull to refresh
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
                     #selector(handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.red
        
        return refreshControl
    }()
    // MARK: - View Life cycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadView()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        loadView()
    }
    
    private func loadView() {
        loadViewFromXib()
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        configureView()
    }

    // MARK: - Configure List View Appearance
    private func configureView() {
        registerCell()
        newsListViewModel.topStoryServiceDelegate = self
        newsListTableView.estimatedRowHeight = 30
        newsListTableView.rowHeight = UITableView.automaticDimension
        newsListTableView.tableFooterView = UIView() // Hide Extra Empty cell
        /// Add Pull to refresh to Table View
        newsListTableView.addSubview(refreshControl)
        if Connectivity.isNetworkAvailable {
            newsListViewModel.getTopStories()
        } else {
            delegate?.success(errorMessgae: "NO_INTERNET".localized)
        }
    }
    
    /// Get The Top Stor id list
    func getTopStory() {
        if Connectivity.isNetworkAvailable {
            newsListViewModel.getTopStories()
        } else {
            delegate?.success(errorMessgae: "NO_INTERNET".localized)
        }
    }
    
    /// Register Table View Cell
    private func registerCell() {
        newsListTableView.register(UINib.init(nibName: Constant.CellIdentifier.hackerNewsTableViewCell, bundle: nil), forCellReuseIdentifier: Constant.CellIdentifier.hackerNewsTableViewCell)
    }

    // MARK: - FIlter the story item on the basis of user query and reload the Tableview
    private func reloadTableViewForFilteredStory(queryString: String) {
        currentTopStories.removeAll()
        for (_,storyItem) in topStories {
            if let storyTitle = storyItem.title,storyTitle.lowercased().range(of: queryString.trim().lowercased()) != nil  {
                currentTopStories.append(storyItem)
            }
        }
        newsListTableView.reloadSections([1], with: .none)
    }
    
    @objc private func handleRefresh(_ refreshControl: UIRefreshControl) {
        cellDataRequestStatus = [:]
        topStories = [:]
        currentTopStories = []
        newsListTableView.reloadData()
        getTopStory()
    }
}

// MARK: - Extension for UITableViewDelegate and UITableViewDataSource  Methods
extension HackerNewsListView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            let headerView = SearchWidget()
            headerView.delegate = self
            return headerView
        }
        return nil
    }
}

// MARK: - Extension for Tableview data source methods
extension HackerNewsListView: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var numberOfRows = 0
        if topStoryIds.count >= 20 && !isFilter && section != 0{
            numberOfRows = 20
        } else if isFilter && section != 0 {
            numberOfRows = currentTopStories.count
        } else if !isFilter && section != 0 {
            numberOfRows = topStoryIds.count
        }
        return numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell?
        if indexPath.section == 1 {
            let tableViewCell = tableView.dequeueReusableCell(withIdentifier: Constant.CellIdentifier.hackerNewsTableViewCell, for: indexPath) as! HackerNewsTableViewCell
            if let isLoaded = cellDataRequestStatus[indexPath.row], !isFilter {
                if isLoaded && topStories[indexPath.row] != nil {
                    tableViewCell.updateView(topStory: topStories[indexPath.row], indexPath: indexPath)
                    return tableViewCell
                } else {
                    tableViewCell.updateView(topStory: nil, indexPath: indexPath)
                }
            } else if isFilter && currentTopStories.indices.contains(indexPath.row) {
                tableViewCell.updateView(topStory: currentTopStories[indexPath.row], indexPath: indexPath)
                return tableViewCell
            }
            tableViewCell.updateContentView(itemId: topStoryIds[indexPath.row], indexPath: indexPath, isRequestSuccess: false)
            tableViewCell.delegate = self
            cell = tableViewCell
        }
        guard let tableViewCell = cell else { return UITableViewCell() }
        return tableViewCell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let headerHeight: CGFloat = section == 0 ? 50 : 1
        return headerHeight
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !isFilter  {
            if let storyItem = topStories[indexPath.row] {
                delegate?.didSelectStory(storyItem: storyItem)
            }
        } else if isFilter{
            delegate?.didSelectStory(storyItem: currentTopStories[indexPath.row])
        }
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
}

// MARK: - Extension for SearchWidgetDelegate methods

extension HackerNewsListView: SearchWidgetDelegate {
    
    func textFieldResign() {
        
    }
    
    func textFieldShouldChangeString(queryString: String) {
        if queryString.isEmpty {
            currentTopStories.removeAll()
            isFilter = false
            newsListTableView.reloadData()
        } else {
            isFilter = true
            reloadTableViewForFilteredStory(queryString: queryString)
        }
    }
}

// MARK: - Extension for Service delegate
extension HackerNewsListView: ModelServiceDelegate {
    func gateWayError() {
        refreshControl.endRefreshing()
        delegate?.gateWayError()
    }
    
    
    func didFinishWithError(errorMessage: String) {
        refreshControl.endRefreshing()
        delegate?.success(errorMessgae: errorMessage)
    }
    
    func didFinishWithSuccess(object: CodableInit?) {
        refreshControl.endRefreshing()
        guard let topStory = object as? TopStory, let topIds =  topStory.id else {
            delegate?.success(errorMessgae: nil)
            return
        }
        delegate?.success(errorMessgae: nil)
        topStoryIds = topIds
        newsListTableView.reloadData()
    }
}

// MARK: - Extension for HackerNewsTableViewCellDelegate methods
extension HackerNewsListView: HackerNewsTableViewCellDelegate {
    func didFinishWithSuccess(topStory: StoryItem, indexPath: IndexPath) {
        if cellDataRequestStatus[indexPath.row] == nil{
            cellDataRequestStatus[indexPath.row] = true
            topStories[indexPath.row] = topStory
            newsListTableView.beginUpdates()
            if let cell  = newsListTableView.cellForRow(at: indexPath) as? HackerNewsTableViewCell {
                cell.updateView(topStory: topStory, indexPath: indexPath)
            }
            newsListTableView.endUpdates()
        }
    }
}

