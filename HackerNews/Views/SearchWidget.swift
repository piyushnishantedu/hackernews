//
//  SearchWidget.swift
//  HoneyGoFood
//
//  Created by Piyush Nishant on 17/11/19.
//  Copyright © 2019 Piyush Nishant. All rights reserved.
//

import UIKit
protocol SearchWidgetDelegate: class {
    func textFieldShouldChangeString(queryString: String)
    func textFieldResign()
}

class SearchWidget: UIView {

    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchImageView: UIImageView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var cancelButtonWidthConstraint: NSLayoutConstraint!
    private var originalWidth: CGFloat = 0
    /// Serach Widget Delegate to handel Serach View event
    weak var delegate: SearchWidgetDelegate?
    
    // MARK: - View Lifecycle Methods
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadView()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        loadView()
    }
    
    private func loadView() {
        Bundle.main.loadNibNamed("SearchWidget", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        configureView()
    }

    
    /// Configure View Details
    private func configureView() {
        mainView.layer.borderColor = UIColor.lightGray.cgColor
        mainView.layer.borderWidth = 0
        mainView.layer.cornerRadius = 6
        searchTextField.delegate = self
        searchTextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(doneButtonClicked))
        originalWidth = cancelButtonWidthConstraint.constant
        cancelButton.isHidden = true
        cancelButtonWidthConstraint.constant = 1
    }
    
    
    /// Key board Done button action event
    /// - Parameter sender: UITextfield
    @objc func doneButtonClicked(_ sender: UITextField) {
        searchTextField.placeholder = sender.text
        if let searchText = sender.text {
            changeWidhthOfCancelButton(isShow: true)
            delegate?.textFieldShouldChangeString(queryString: searchText.trim())
        }
        sender.resignFirstResponder()
    }
    
    
    /// Cancel Button action when use Tap on Cancel button empty text field text
    /// - Parameter sender: UIButton reference
    @IBAction func cancelButtonAction(_ sender: UIButton) {
        searchTextField.placeholder = "STORY_NAME".localized
        searchTextField.text = ""
        changeWidhthOfCancelButton(isShow: true)
        searchTextField.resignFirstResponder()
        delegate?.textFieldShouldChangeString(queryString: "")
    }
    
    
    /// Change the width of Cancel button for Animation
    /// - Parameter isShow: Bool variable which show and hide cancel Button
    private func changeWidhthOfCancelButton(isShow: Bool) {
        let width = isShow ? 1 : originalWidth
        cancelButtonWidthConstraint.constant = width
        cancelButton.isHidden = isShow
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0, options: .curveEaseInOut, animations: { [weak self] in
            self?.layoutIfNeeded()
        }, completion: nil)
    }
    
}

// MARK: - UITextField Delegate Methods for Filter the List
extension SearchWidget: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == searchTextField {
            searchTextField.resignFirstResponder()
            if let searchText = textField.text {
                changeWidhthOfCancelButton(isShow: true)
                searchTextField.resignFirstResponder()
                delegate?.textFieldShouldChangeString(queryString: searchText.trim())
            }
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        changeWidhthOfCancelButton(isShow: false)
    }
}
