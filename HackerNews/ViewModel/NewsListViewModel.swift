//
//  NewsListViewModel.swift
//  HackerNews
//
//  Created by Piyush Nishant on 10/12/19.
//  Copyright © 2019 Piyush Nishant. All rights reserved.
//

import Foundation
final class NewsListViewModel {
    weak var topStoryServiceDelegate: ModelServiceDelegate?
    
    
    /// Get Top stories
    func getTopStories() {
        StoryService.story.send(TopStory.self, then: storyResponse)
    }
    
    /// Store Data into data base
    /// - Parameter topStory: Top story
    private func storeStoryInDataBase(topStory: TopStory) {
        DatabaseManager.sharedManager.insertTopstoryData(topStoryId: topStory) { [weak self] (isSuccess) in
            if isSuccess {
                self?.topStoryServiceDelegate?.didFinishWithSuccess(object: topStory)
                
            } else {
                let errormessage = "DB_ERROR".localized
                self?.topStoryServiceDelegate?.didFinishWithError(errorMessage: errormessage)
            }
        }
    }
}

// MARK: Extension for NewsListViewModel to handel the response
extension NewsListViewModel {
    var storyResponse: HandleResponse<TopStory> {
            return { [weak self] (response) in
                switch response {
                case .failure(let error):
                    // TODO: - Handle error as you want, printing isn't handling.
                    switch error {
                    case is ApplicationError:
                        switch error as! ApplicationError {
                        case .netWorkError(let type):
                            let errorMessage = "\(type.localizedDescription)"
                            self?.topStoryServiceDelegate?.didFinishWithError(errorMessage: errorMessage)
                        case .apiError(_ , let errorMessage):
                            let errormessage = errorMessage ?? "TECHNICCAL_ERROR".localized
                            self?.topStoryServiceDelegate?.didFinishWithError(errorMessage: errormessage)
                        case .gateWayError( _):
                            self?.topStoryServiceDelegate?.gateWayError()
                        }
                    default:
                        self?.topStoryServiceDelegate?.didFinishWithError(errorMessage: "TECHNICCAL_ERROR".localized)
                    }
                case .success(let topStory):
//                    self?.storeStoryInDataBase(topStory: topStory)
                    self?.topStoryServiceDelegate?.didFinishWithSuccess(object: topStory)
                    
                }
            }
        }
}
