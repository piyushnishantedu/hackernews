//
//  HackerNewsTableViewCellModel.swift
//  HackerNews
//
//  Created by Piyush Nishant on 10/12/19.
//  Copyright © 2019 Piyush Nishant. All rights reserved.
//

import Foundation
final class HackerNewsTableViewCellModel {
    /// Model Service Delegate which notifiy the class when response comes from API Service
    weak var itemServiceDelegate: ModelServiceDelegate?
    
    // MARK: - Get StoryItem Detail With Item Id
    
    /// Get Story Item Details
    /// - Parameter itemId: Item Id To get STory Details
    func getTopStoryItem(itemId: Int64)  {
        StoryService.item(itemId: itemId).send(StoryItem.self, then: itemResponse)
    }
    
    
    /// Get Formated Author name to display on cell
    /// - Parameter authorName: String
    func getAuthorName(authorName: String) -> String {
        let formatedAuthor = "AUTHOR".localized + authorName
        return formatedAuthor
    }
    
    
    /// Get title with under score
    /// - Parameter title: String
    func getTitle(title: String?) -> String {
        let title = title ?? ""
        return title
    }
    
    
    /// Get Formated Number of comment
    /// - Parameter comments: Array of Comment
    func getComment(comments: [Comment]?) -> String {
        let comment = "\(comments?.count ?? 0) " + "COMMENTS".localized
        return comment
    }
}

// MARK: - Extension of HackerNewsTableViewCellModel to handel StoryItem Response
extension HackerNewsTableViewCellModel {
    var itemResponse: HandleResponse<StoryItem> {
            return { [weak self] (response) in
                switch response {
                case .failure(let error):
                    switch error {
                    case is ApplicationError:
                        switch error as! ApplicationError {
                        case .netWorkError(let type):
                            let errorMessage = "\(type.localizedDescription)"
                            self?.itemServiceDelegate?.didFinishWithError(errorMessage: errorMessage)
                        case .apiError(_ , let errorMessage):
                            let errormessage = errorMessage ?? "TECHNICCAL_ERROR".localized
                            self?.itemServiceDelegate?.didFinishWithError(errorMessage: errormessage)
                        case .gateWayError( _):
                            self?.itemServiceDelegate?.gateWayError()
                        }
                    default:
                        self?.itemServiceDelegate?.didFinishWithError(errorMessage: "TECHNICCAL_ERROR".localized)
                    }
                case .success(let topStory):
                    self?.itemServiceDelegate?.didFinishWithSuccess(object: topStory)
                    
                }
            }
        }
}
