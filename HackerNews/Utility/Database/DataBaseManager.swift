//
//  DataBaseManager.swift
//  HackerNews
//
//  Created by Piyush Nishant on 10/12/19.
//  Copyright © 2019 Piyush Nishant. All rights reserved.
//

import Foundation
import CoreStore
final class DatabaseManager: NSObject {
    static let sharedManager = DatabaseManager()
    private var dataStack: DataStack!
    override init() {
        super.init()
        setUpCoreDataStack()
    }
    
    // MARK: - Set up core data stack
    private func setUpCoreDataStack() {
        // Initialize the coredata
        do {
            dataStack = DataStack(xcodeModelName: "HackerNews",
                                               bundle: Bundle.main,
                                               migrationChain: ["HackerNews"])

            let bundleName = (Bundle.main.object(forInfoDictionaryKey: "CFBundleName") as? String) ?? "CoreData"
            let sqliteFileName = bundleName + ".sqlite"
            let sqliteStorage = SQLiteStore(fileName: sqliteFileName,
                                            localStorageOptions: .allowSynchronousLightweightMigration
            )
            try dataStack.addStorageAndWait(sqliteStorage)
            // Just after migration correct the data
//            CoreStoreDefaults.dataStack = dataStack
           
        } catch {
            print("Error in data base Initialization")
        }
    }
    
    // MARK: - Fetch All Top storied Ids
    func fetchTostoriesIds() -> TopStory{
        var storyIds = [Int64]()
        let topStory = TopStory()
        do {
            let topdbStories = try dataStack.fetchAll(From<StoryEntry>())
            for story in topdbStories {
                storyIds.append(story.id)
            }
            topStory.id = storyIds
            return topStory
        } catch {
            print("Not able to fetch Top story")
            return topStory
        }
    }
    
    func insertTopstoryData(topStoryId: TopStory, completionHandeler: @escaping (_ isSuccess: Bool) -> Void) {
        var insertedItem = 0
        guard let topIds = topStoryId.id else {
            completionHandeler(false)
            return
        }
        for i in 0..<20 {
            dataStack.perform(
                asynchronous: { (transaction) -> Void in
                    let story = transaction.create(Into<StoryEntry>())
                    story.id = topIds[i]
                },
                completion: { (result) -> Void in
                    switch result {
                    case .success:
                        print("success!")
                        insertedItem = insertedItem + 1
                        if insertedItem == 20 {
                            completionHandeler(true)
                        } else {
                            completionHandeler(false)
                        }
                    case .failure(let error): print(error)
                    }
                }
            )
        }
    }
}
