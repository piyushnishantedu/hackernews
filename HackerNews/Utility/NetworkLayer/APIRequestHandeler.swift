//
//  APIRequestHandeler.swift
//  HackerNews
//
//  Created by Piyush Nishant on 10/12/19.
//  Copyright © 2019 Piyush Nishant. All rights reserved.
//

import Foundation
import Foundation
import Alamofire

/// Response Completion Handler
typealias ResponseObject<T> = ((Result<T,Error>) -> Void)?

/// API Protocol for almofire wrapper
protocol APIRequestHanler: HandleAlamoResponse { }

// MARK: - Extension for executing request
extension APIRequestHanler where Self: RequestBuilder {

    func send<T: CodableInit>(_ decoder: T.Type, progress: ((Progress) -> Void)? = nil, then: ResponseObject<T>) {
            AF.request(self).validate().responseData {(response) in
            self.handleResponse(response, then: then)
        }
    }
    
    func cancelRequest() -> Void {
        let sessionManager = Alamofire.Session.default
        sessionManager.session.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in
            dataTasks.first(where: { $0.originalRequest?.url == self.requestURL})?.cancel()
            uploadTasks.first(where: { $0.originalRequest?.url == self.requestURL})?.cancel()
            downloadTasks.first(where: { $0.originalRequest?.url == self.requestURL})?.cancel()
        }
    }
}
