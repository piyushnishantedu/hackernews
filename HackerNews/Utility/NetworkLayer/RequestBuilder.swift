//
//  RequestBuilder.swift
//  HackerNews
//
//  Created by Piyush Nishant on 10/12/19.
//  Copyright © 2019 Piyush Nishant. All rights reserved.
//

import Foundation
import Alamofire

protocol RequestBuilder: URLRequestConvertible, APIRequestHanler {
    
    var mainURL: URL { get }
    var requestURL: URL { get }
    // MARK: - Path
    var path: String { get }
    
    // MARK: - Parameters
    var parameters: Parameters? { get }
    
    // MARK: - Methods
    var method: HTTPMethod { get }
    
    // Encoding
    var encoding: ParameterEncoding { get }
    
    // Url request
    var urlRequest: URLRequest { get }
}

// MARK: Extension for Request Builder
extension RequestBuilder {
    var encoding: ParameterEncoding {
        switch method {
        case .get:
            return URLEncoding.default
        default:
            return JSONEncoding.default
        }
    }
    
    var requestURL: URL {
        return mainURL.appendingPathComponent(path)
    }
    
    var urlRequest: URLRequest {
        var request = URLRequest(url: requestURL)
        request.httpMethod = method.rawValue
        return request
    }
    
    // MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        return try encoding.encode(urlRequest, with: parameters)
    }
}

