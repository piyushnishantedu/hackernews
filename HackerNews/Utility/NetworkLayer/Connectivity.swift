//
//  Connectivity.swift
//  HackerNews
//
//  Created by Piyush Nishant on 10/12/19.
//  Copyright © 2019 Piyush Nishant. All rights reserved.
//

import Alamofire
struct Connectivity {
    private init() {}
    /// Check if internet connectivity
    static var isNetworkAvailable: Bool {
        guard let rechabilityManager = NetworkReachabilityManager() else {
            return false
        }
        return rechabilityManager.isReachable
    }
}
