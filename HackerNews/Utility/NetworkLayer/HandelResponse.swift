//
//  HandelResponse.swift
//  HackerNews
//
//  Created by Piyush Nishant on 10/12/19.
//  Copyright © 2019 Piyush Nishant. All rights reserved.
//

import Foundation
import Alamofire

// MARK: Protocol for generic object model
protocol CodableInit {
    init(data: Data) throws
}

extension CodableInit where Self: Codable {
    init(data: Data) throws {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .useDefaultKeys
        self = try decoder.decode(Self.self, from: data)
    }
}

// Mark: - Application Error
enum ApplicationError: Error {
    case netWorkError(type: HttpError.NetworkError)
    case apiError(errorCode: Int?, errorMessage: String?)
    case gateWayError(errorMessage: String?)
}
/// Error description of Application Error
extension ApplicationError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .netWorkError(let type):
            return type.localizedDescription
        case .apiError(_, let errorMessage):
            return errorMessage
        case .gateWayError(let errorMessage):
            return errorMessage
        }
    }
}
// Mark: - Server Error
enum HttpError: Error {
    enum NetworkError {
        case parsing
        case notFound
        case noInterNet
        case custom(errorCode: Int?, errorDescription: String?)
    }
}
/// Server error localized description
extension HttpError.NetworkError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .parsing:
            return "Json Parsing Error"
        case .notFound:
            return "URL not found"
        case .noInterNet:
            return "NO_INTERNET".localized
        case .custom(_, let errorDescription):
            return errorDescription
        }
    }
}



typealias HandleResponse<T: CodableInit> = (Result<T, Error>) -> Void

protocol HandleAlamoResponse {
    /// Handles request response, never called anywhere but APIRequestHandler
    ///
    /// - Parameters:
    ///   - response: response from network request, for now alamofire Data response
    ///   - completion: completing processing the json response, and delivering it in the completion handler
    func handleResponse<T: CodableInit>(_ response: AFDataResponse<Data>, then: ResponseObject<T>)
}

extension HandleAlamoResponse {
    
    func handleResponse<T: CodableInit>(_ response: AFDataResponse<Data>, then: ResponseObject<T>) {
        guard let statusCode = response.response?.statusCode, statusCode == 200 else {
            let responseStatusCode = response.response?.statusCode ?? 404
            if (responseStatusCode == Constant.ServerErrorCode.badGateWay.rawValue) {
                // GATEWAY_TIMEOUT
                let error = ApplicationError.gateWayError(errorMessage: "GATEWAY_TIMEOUT".localized)
                then?(.failure(error))
                return
            }
            let error = ApplicationError.netWorkError(type: .custom(errorCode: responseStatusCode, errorDescription: "TECHNICCAL_ERROR".localized))
            then?(.failure(error))
            return
        }
        switch response.result {
        case .failure(let error):
            then?(.failure(error))
        case .success(let value):
            do {
                let responseJson = try JSONSerialization.jsonObject(with: value, options: .allowFragments) as? [String:Any]
                if let apiStatusCode = responseJson?["statusCode"] as? Int, apiStatusCode != 200 {
                    let message =  responseJson?["message"] as? String ?? "TECHNICCAL_ERROR".localized
                    let error = ApplicationError.apiError(errorCode: nil, errorMessage: message)
                    then?(.failure(error))
                    return
                }
                
                let modules = try T(data: value)
                then?(.success(modules))
            }catch {
                then?(.failure(error))
            }
        }
    }
    
}

