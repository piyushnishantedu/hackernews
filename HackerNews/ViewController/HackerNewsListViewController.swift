//
//  HackerNewsListViewController.swift
//  HackerNews
//
//  Created by Piyush Nishant on 10/12/19.
//  Copyright © 2019 Piyush Nishant. All rights reserved.
//

import UIKit

class HackerNewsListViewController: UIViewController, ErrorViewRepresentable, LoaderRepresentable {
    @IBOutlet var hackerNewsListView: HackerNewsListView!
    var errorMainView: ErrorView?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
    }
    
    // MARK: - View Life cycle methods
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
        navigationController?.setToolbarHidden(true, animated: false)
    }
    
    // MARK: - Configure the View Content
    private func configureView() {
         addErrorView()
        hackerNewsListView.delegate = self
        if Connectivity.isNetworkAvailable {
            hackerNewsListView.isHidden = true
            showLoader()
        } else {
            showAndHideErrorView(isShow: true, messgae: "NO_INTERNET".localized)
            hackerNewsListView.isHidden = true
        }
    }
    
    // MARK: Go to Story detail View Controller
    
    /// Go to Story details when user tap on Story Item
    /// - Parameter storyItem: Story Item
    private func goToStoryDetailViewController(with storyItem: StoryItem) {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        guard let storyDetailViewController = mainStoryboard.instantiateViewController(withIdentifier: Constant.ViewControllerIdentifier.storyDetailViewController) as? StoryDetailViewController else { return }

        storyDetailViewController.requestUrlString = storyItem.url
        navigationController?.pushViewController(storyDetailViewController, animated: true)
    }
    
    
    /// Error vies has retry button to hit the api again
    func retryButtonAction() {
        showAndHideErrorView(isShow: false)
       if Connectivity.isNetworkAvailable {
           hackerNewsListView.isHidden = true
           showLoader()
           hackerNewsListView.getTopStory()
       } else {
           showAndHideErrorView(isShow: true, messgae: "NO_INTERNET".localized)
           hackerNewsListView.isHidden = true
       }
    }
}

// MARK: - Extension for News list View delegate Methods
extension HackerNewsListViewController: HackerNewsListViewDelegate {
    
    /// Success when the request is completed withou error
    /// - Parameter errorMessgae: Optional message
    func success(errorMessgae: String?) {
        if let errorMsg = errorMessgae {
            showAndHideErrorView(isShow: true, messgae: errorMsg)
            hackerNewsListView.isHidden = true
            hideLoader()
        } else {
            showAndHideErrorView(isShow: false, messgae: errorMessgae)
            hackerNewsListView.isHidden = false
            hideLoader()
        }
    }
    
    
    /// When there is gate way error show the error screen to user
    func gateWayError() {
        showAndHideErrorView(isShow: true, messgae: "GATEWAY_TIMEOUT".localized)
        hackerNewsListView.isHidden = true
        hideLoader()
    }
    
    
    /// Navuigate to Story detail screen when user tap on Story item
    /// - Parameter storyItem: Story Item
    func didSelectStory(storyItem: StoryItem) {
        goToStoryDetailViewController(with: storyItem)
    }
}
