//
//  StoryDetailViewController.swift
//  HackerNews
//
//  Created by Piyush Nishant on 11/12/19.
//  Copyright © 2019 Piyush Nishant. All rights reserved.
//

import UIKit
import WebKit

final class StoryDetailViewController: UIViewController, AlertViewRepresentable, ErrorViewRepresentable, LoaderRepresentable {

    @IBOutlet weak var storyDetailWebView: WKWebView!
    @IBOutlet weak var backwardButton: UIButton!
    @IBOutlet weak var forwardButton: UIButton!
    
    var errorMainView: ErrorView?
    var requestUrlString: String?
    
    // MARK: - View Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
        loadRequest(with: requestUrlString)
        navigationItem.title = ""
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    /// Forward Button Action
    @IBAction func forwardButtonAction(_ sender: UIButton) {
        storyDetailWebView.goForward()
    }
    
    /// Bacckward Button Action
    @IBAction func backwardButtonAction(_ sender: UIButton) {
        storyDetailWebView.goBack()
    }
    
    /// Configure the View Content
    private func configureView() {
        storyDetailWebView.navigationDelegate = self
        addErrorView()
        backwardButton.isEnabled = false
        forwardButton.isEnabled = false
        self.navigationController?.navigationBar.topItem?.title = "TOP_STORIES_TITLE".localized
    }
    
    // MARK: Retry Button action on Error view
    func retryButtonAction() {
        showAndHideErrorView(isShow: false)
        if Connectivity.isNetworkAvailable {
            storyDetailWebView.isHidden = true
            loadRequest(with: requestUrlString)
        } else {
            showAndHideErrorView(isShow: true, messgae: "NO_INTERNET".localized)
            storyDetailWebView.isHidden = true
            hideLoader()
        }
    }
    
    // MARK: - Load the request for web view
    
    /// Load Web view request using the request url
    /// - Parameter urlString: Url String
    private func loadRequest(with urlString: String?) {
        if Connectivity.isNetworkAvailable {
            storyDetailWebView.isHidden = false
            showLoader()
            if let urlString = urlString, let url = URL(string: urlString) {
                let request = URLRequest(url: url)
                storyDetailWebView.load(request)
            } else {
                hideLoader()
                showAndHideErrorView(isShow: true, messgae: "SOME_THING_WENT_RONG".localized)
            }
        } else {
            showAndHideErrorView(isShow: true, messgae: "NO_INTERNET".localized)
            storyDetailWebView.isHidden = true
            hideLoader()
        }
    }
}

// MARK: - Extension for WKNavigationDelegate methods
extension StoryDetailViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        hideLoader()
        if webView.canGoBack {
            backwardButton.isEnabled = true
        } else {
            backwardButton.isEnabled = false
        }
        if webView.canGoForward{
            forwardButton.isEnabled = true
        } else {
             forwardButton.isEnabled = false
        }
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        showAndHideErrorView(isShow: true, messgae: "SOME_THING_WENT_RONG".localized)
        hideLoader()
    }
}
