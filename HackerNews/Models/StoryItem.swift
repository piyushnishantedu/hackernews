//
//  Item.swift
//  HackerNews
//
//  Created by Piyush Nishant on 10/12/19.
//  Copyright © 2019 Piyush Nishant. All rights reserved.
//

import Foundation
final class StoryItem: Codable, CodableInit {
    let author: String
    let descendants: Int64?
    let id: Int64?
    let score: Int?
    var title: String?
    let type: String?
    let url: String?
    let comments:[Comment]?
    enum CodingKeys: String, CodingKey {
        case author = "by"
        case descendants = "descendants"
        case score = "score"
        case id = "id"
        case title = "title"
        case type = "type"
        case url = "url"
        case kids = "kids"
    }
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        author = try container.decode(String.self, forKey: .author)
        descendants = try container.decode(Int64.self, forKey: .descendants)
        id = try container.decode(Int64.self, forKey: .id)
        score = try container.decode(Int.self, forKey: .score)
        title = try container.decode(String.self, forKey: .title)
        url = try container.decode(String.self, forKey: .url)
        type = try container.decode(String.self, forKey: .type)
        comments = try container.decodeIfPresent([Comment].self, forKey: .kids) ?? nil
    }

    func encode(to encoder: Encoder) throws {

    }
}
