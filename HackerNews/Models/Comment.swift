//
//  Comment.swift
//  HackerNews
//
//  Created by Piyush Nishant on 12/12/19.
//  Copyright © 2019 Piyush Nishant. All rights reserved.
//

import Foundation
final class Comment: Codable, CodableInit {
    /// List of Comment Ids
    var idList: [Int64]?
    init(from decoder: Decoder) throws {}
    init(data: Data) throws {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .useDefaultKeys
        idList = try decoder.decode([Int64].self, from: data)
    }
    init() {}
}
