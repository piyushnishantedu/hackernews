//
//  TopStory.swift
//  HackerNews
//
//  Created by Piyush Nishant on 10/12/19.
//  Copyright © 2019 Piyush Nishant. All rights reserved.
//

import Foundation
final class TopStory: Codable, CodableInit {
    ///List of ids
    var id: [Int64]?
    init(from decoder: Decoder) throws {}
    init(data: Data) throws {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .useDefaultKeys
        id = try decoder.decode([Int64].self, from: data)
    }
    init() {}
}
