//
//  ConfigurationManager.swift
//  HackerNews
//
//  Created by Piyush Nishant on 10/12/19.
//  Copyright © 2019 Piyush Nishant. All rights reserved.
//

import Foundation

final class ConfigurationManager: NSObject {
    var environment: Environment?
    
    // Singleton method
    static let sharedManager: ConfigurationManager = {
        let instance = ConfigurationManager()
        // setup code
        return instance
    }()
    
    override init() {
        super.init()
        setUp()
    }
    
    /// Initialize the Configuration Manager
    private func setUp() {
        //load the environment settings
        environment = getEnvironmentConfiguration()
    }
    
    /// Get Environment Setting
    private func getEnvironmentConfiguration() -> Environment {
        let prodEnv = production
        return prodEnv
    }
}

