//
//  Environment.swift
//  HackerNews
//
//  Created by Piyush Nishant on 10/12/19.
//  Copyright © 2019 Piyush Nishant. All rights reserved.
//

import Foundation
import Foundation
struct Environment {
    var name = ""
    var baseUrlPath = ""
}
/// prod Server Configuration
let production = Environment(name: "production", baseUrlPath: Constant.APIService.baseUrl)
