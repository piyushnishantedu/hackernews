//
//  StoryServiceManager.swift
//  HackerNews
//
//  Created by Piyush Nishant on 10/12/19.
//  Copyright © 2019 Piyush Nishant. All rights reserved.
//

import Foundation
import Foundation
import Alamofire
/// Story Service to get the Top story List and story details
enum StoryService: RequestBuilder {
    case story
    case item(itemId: Int64)
    var path: String {
        switch self {
        case .story:
            return Constant.APIService.topStories
        case .item(let itemId):
            return Constant.APIService.item + "\(itemId).json"
        }
    }
    
    var mainURL: URL  {
        guard let env = ConfigurationManager.sharedManager.environment, let apiBase = URL(string: env.baseUrlPath) else {
            fatalError("URL is not Properly")
        }
        return apiBase
    }
    
    var parameters: Parameters? {
        return nil
    }
    
    var method: HTTPMethod {
        return .get
    }
}

