//
//  Constant.swift
//  HackerNews
//
//  Created by Piyush Nishant on 10/12/19.
//  Copyright © 2019 Piyush Nishant. All rights reserved.
//

import Foundation
struct Constant {
    private init() {}
    
    // MARK: - Cell Identifiers
    struct CellIdentifier {
        private init(){}
        static let hackerNewsTableViewCell = "HackerNewsTableViewCell"
    }
    
    // MARK: - Viewcontroller Identifier
    struct ViewControllerIdentifier {
        private init() {}
        static let hackerNewsListViewController = "HackerNewsListViewController"
        static let storyDetailViewController = "StoryDetailViewController"
    }
    
    /// API Service Path
    struct APIService {
        private init() { }
        static let baseUrl = "https://hacker-news.firebaseio.com"
        static let item = "/v0/item/"
        static let topStories = "/v0/topstories.json"
    }
    
    // Server Error code
    enum ServerErrorCode: Int {
        case badGateWay = 504
        case notfound = 404
    }
}
