//
//  String+Extension.swift
//  HackerNews
//
//  Created by Piyush Nishant on 10/12/19.
//  Copyright © 2019 Piyush Nishant. All rights reserved.
//
import Foundation
extension String {
    /// Get the localized string from language string file
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
    
    /// Trim the string having spaces at the start and end of string
    func trim() -> String {
        return (self as NSString).trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
    }
    
    /// Replace white spaces to underscore
    func getUnderScoreString() -> String {
        let newString = self.lowercased().replacingOccurrences(of: " ", with: "_")
        return newString
    }
}

