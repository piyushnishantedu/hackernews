//
//  ModelServiceDelegate.swift
//  HackerNews
//
//  Created by Piyush Nishant on 10/12/19.
//  Copyright © 2019 Piyush Nishant. All rights reserved.
//

import Foundation
/// Model Service Delegate Protocol
protocol ModelServiceDelegate: class {
    func didFinishWithError(errorMessage: String)
    func didFinishWithSuccess(object: CodableInit?)
    func gateWayError()
}
