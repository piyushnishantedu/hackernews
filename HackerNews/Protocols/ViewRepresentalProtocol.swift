//
//  ViewRepresentalProtocol.swift
//  HackerNews
//
//  Created by Piyush Nishant on 10/12/19.
//  Copyright © 2019 Piyush Nishant. All rights reserved.
//

import UIKit

// MARK: - Protocols for loading view using xib 
protocol InstantiateFromXIBRepresentable {
    func loadViewFromXib()
}

// MARK: Extension for InstantiateFromXIBRepresentable
extension InstantiateFromXIBRepresentable where Self: UIView {
    
    // MARK: Load the view from Xib file
    func loadViewFromXib() {
        let name = String(describing: type(of: self))
        Bundle.main.loadNibNamed(name, owner: self, options: nil)
    }
}
