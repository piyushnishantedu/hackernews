//
//  AlertViewRepresentable.swift
//  HackerNews
//
//  Created by Piyush Nishant on 11/12/19.
//  Copyright © 2019 Piyush Nishant. All rights reserved.
//

import UIKit
/// Alert View represent view
protocol AlertViewRepresentable: class {
}

// MARK: Extension for AlertViewRepresentable
extension AlertViewRepresentable where Self: UIViewController {
    
    // MARK: Show the Alert view
    func showAlert(title: String, message: String?, cancelButtonTitle: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: cancelButtonTitle , style: UIAlertAction.Style.default, handler: { _ in
            //Cancel Action
            self.dismiss(animated: true, completion: nil)
        }))
    }
}
