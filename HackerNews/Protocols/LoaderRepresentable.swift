//
//  LoaderRepresentable.swift
//  HackerNews
//
//  Created by Piyush Nishant on 11/12/19.
//  Copyright © 2019 Piyush Nishant. All rights reserved.
//

import UIKit

/// Show and Hide Loader on API request and resonse
protocol LoaderRepresentable: class {
    
}
fileprivate enum LoaderTag: Int {
    case tag = -10001
}

// MARK: Extension for LoaderRepresentable
extension LoaderRepresentable where Self: UIViewController {
    /// Add Loader the UIViewcontroller view as subview
    private func addLoader(isHidden: Bool) {
        let loader = UIView(frame: view.bounds)
        loader.isHidden = isHidden
        loader.tag = LoaderTag.tag.rawValue
        loader.backgroundColor = UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
        activityIndicator.startAnimating()
        activityIndicator.center = loader.center
        DispatchQueue.main.async { [unowned self] in
            loader.addSubview(activityIndicator)
            self.view.addSubview(loader)
            self.view.bringSubviewToFront(loader)
        }
    }
    
    // MARK: Show Loader
    func showLoader() {
        if let loader = view.viewWithTag(LoaderTag.tag.rawValue) {
            loader.isHidden = false
            view.bringSubviewToFront(loader)
        } else {
            addLoader(isHidden: false) /// Add Loader when it is not added in View
        }
    }
    
    // MARK: Hide Loader
    func hideLoader() {
        if let loader = view.viewWithTag(LoaderTag.tag.rawValue) {
            loader.isHidden = true
            view.bringSubviewToFront(loader)
        } else {
            addLoader(isHidden: true) /// Add Loader when it is  not added in View
        }
    }
    
}
