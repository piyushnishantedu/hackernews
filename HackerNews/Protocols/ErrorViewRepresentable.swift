//
//  ErrorViewRepresentable.swift
//  HackerNews
//
//  Created by Piyush Nishant on 11/12/19.
//  Copyright © 2019 Piyush Nishant. All rights reserved.
//

import UIKit
/// Show and Hide Error
protocol  ErrorViewRepresentable: ErrorViewDelegate {
    var errorMainView: ErrorView? {get set}
}

// MARK: Extension for ErrorViewRepresentable
extension ErrorViewRepresentable where Self: UIViewController{
    /// Get the error view if it is allocated other wise allocate it the get it
    private var errorView: ErrorView? {
        guard let errorview = self.errorMainView else {
            self.errorMainView = ErrorView(frame: UIScreen.main.bounds)
            return self.errorMainView
        }
        return errorview
    }
    
    // MARK: Add error view to the UIViewcontroller view
    func addErrorView() {
        guard let errorview = errorView else { return }
        errorview.isHidden = true
        errorview.delegate = self
        view.addSubview(errorview)
    }
    
    // MARK: Show and hide the error view
    func showAndHideErrorView(isShow: Bool, messgae: String? = nil) {
        errorView?.isHidden = !isShow
        errorView?.errorLabel.text = messgae ?? ""
    }
}

